import React from 'react';
import PropTypes from 'prop-types';
import './ExpandImage.scss';


class ExpandImage extends React.Component {
    static propTypes = {
        url: PropTypes.string,
        title: PropTypes.string,
        isDisplay: PropTypes.string
    };
    constructor(props) {
        super(props);
        this.closeImage = this.closeImage.bind(this);
        this.state = {
            url: this.props.url,
            isDisplay: this.props.isDisplay,
            title: this.props.title
        };
    }

    shouldComponentUpdate(nextState) {
        if (nextState.isDisplay === 'block') {
            this.setState({ isDisplay: 'block' })
            return true;
        } else {
            this.setState({ isDisplay: 'none' })
            return true;
        }

    }

    closeImage() {
        this.setState({ isDisplay: 'none' });
    }

    render() {
        return (
            <div className="expend" style={{ display: this.state.isDisplay }}>
                <img src={this.state.url} />
                <span className="close" onClick={this.closeImage}>&times;</span>
                <p> {this.state.title}</p>
            </div >
        )
    }

}
export default ExpandImage;