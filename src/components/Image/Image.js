import React from 'react';
import PropTypes from 'prop-types';
import ExpandImage from './ExpandImage/ExpandImage';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number,
    copyImage: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.imageFiter = this.imageFiter.bind(this);
    this.expandImage = this.expandImage.bind(this);
    this.cloneImage = this.cloneImage.bind(this);
    this.responsive = this.responsive.bind(this);
    this.state = {
      size: 200,
      filter: [
        'grayscale(100%)',
        'saturate(800%)',
        'brightness(200%)',
        'hue-rotate(90deg)',
        'invert(100%)'
      ],
      randomIndex: null,
      expandImageComp: null
    };
  }

  calcImageSize() {
    const { galleryWidth } = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = ((galleryWidth - 17) / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
    this.setState({
      randomIndex: null,
      expandImageComp: null
    });
    window.addEventListener('resize', this.responsive);
  }

  responsive() {
    this.setState({});
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  imageFiter() {
    this.setState({
      randomIndex: Math.floor(Math.random() * this.state.filter.length)
    })
  }

  expandImage() {
    let imageUrl = this.urlFromDto(this.props.dto);
    let uniqeKey = 'ExpandImage-' + this.props.dto.id;
    let title = this.props.dto.title;

    this.setState({
      randomIndex: null,
      expandImageComp: <ExpandImage isDisplay={'block'}
        key={uniqeKey}
        url={imageUrl}
        title={title} />
    });
  }

  cloneImage() {
    this.setState({
      randomIndex: null,
      expandImageComp: null
    });
    this.props.copyImage(this.props.dto);
  }

  render() {
    return (
      <div
        className="image-root"
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px',
          filter: this.state.filter[this.state.randomIndex]
        }}
      >
        <div>
          <FontAwesome className="image-icon" name="clone" title="clone" onClick={this.cloneImage} />
          <FontAwesome className="image-icon" name="filter" title="filter" onClick={this.imageFiter} />
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={this.expandImage} />
          {this.state.expandImageComp}
        </div>
      </div>
    );
  }
}

export default Image;
