import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.addImagesWhenScrollEnd = this.addImagesWhenScrollEnd.bind(this);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      isLoading: 'none',
      tagSearching: null,
      page: 1,
      timeout: null
    };
  }

  getGalleryWidth() {
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getImages(tag) {
    const { tagSearching } = this.state;

    this.setState({ isLoading: 'block' });

    if (tagSearching !== tag) {
      this.setState({ tagSearching: tag });
      this.setState({ images: [] });
    } else {
      this.setState({ page: this.state.page + 1 });
    }

    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&page=${this.state.page}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          if (this.state.page === 1) {
            this.setState({ images: res.photos.photo });
          }
          else {
            this.setState({ images: [...this.state.images, ...res.photos.photo] })
          }
        }
        this.setState({ isLoading: 'none' });
      });
  }

  componentDidMount() {
    if (this.state.timeout) {
      clearTimeout(this.timeout);
    }

    this.setState({
      timeout: setTimeout(() => {
        this.getImages(this.props.tag);
      }, 500)
    })

    this.setState({
      galleryWidth: document.body.clientWidth
    });
    window.addEventListener('scroll', this.addImagesWhenScrollEnd);
  }

  componentWillReceiveProps(props) {
    if (this.state.timeout) {
      clearTimeout(this.timeout);
    }
    this.setState({
      timeout: setTimeout(() => {
        this.getImages(props.tag);
      }, 500)
    })
  }

  addImagesWhenScrollEnd() {
    if ((window.innerHeight + window.scrollY === document.body.scrollHeight)) {
      this.getImages(this.props.tag);
    }
  }

  copyImage(dto) {
    this.state.images.push(dto);
    this.setState({ images: this.state.images })
  }

  render() {
    return (
      <div className="gallery-root">
        {this.state.images.map((dto, index) => {
          return <Image key={'image-' + dto.id + '-' + index}
            dto={dto}
            galleryWidth={this.state.galleryWidth}
            copyImage={dto => this.copyImage(dto)} />;
        })}
        <i className="fa fa-circle-o-notch fa-spin" style={{
          display: this.state.isLoading,
          fontSize: '100px',
          color: 'gray'
        }}></i>
      </div>
    );
  }
}

export default Gallery;
